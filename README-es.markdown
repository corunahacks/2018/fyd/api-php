# FYD ( Fuck you doodle ) - Api php

Este proyecto da un completo api para el proyecto "fyd", un sistema de votaciones alternativa a doodle.

## Getting Started

Este proyecto usa symfony 4 con api-platform con docker.

### Prerequisitos

Tener instalado docker

```
Docker version 18.06.1-ce
```

## Deployment

Para tener todo el proyecto en local, simplemente tienes que lanzar los contenedores con el comando:

```
sudo docker-compose up

```
Esto te levantará un mysql, un phpmyadmin, un contenedor php, un cliente y un panel de administración.

Puedes acceder desde 
>http://localhost:80XX

Mirando los puertos en el documento docker-compose en la raiz del proyecto.

## Contribuidores

Se trata de un proyecto iniciado en los #coruñaHacks 2018.
